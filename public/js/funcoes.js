/**
 * Created by Sagres 3 on 08/10/2018.
 */
$(document).ready(function () {
    $('.parallax').parallax();

    $('.tap-target').tapTarget();

    $('.fixed-action-btn').floatingActionButton({
        direction: 'left'
    });

});
