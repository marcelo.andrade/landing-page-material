<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/style-comum.css')}}"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<div class="container-fluid">
    <nav class="nav nav-wrapper grey darken-4 logo">
        <a href="#" class="brand-logo">HOME</a>
        <ul class="right">
            <li><a href="">Produtos</a></li>
            <li><a href="">Clientes</a></li>
        </ul>
    </nav>

</div>


<div class="container-fluid">
    @yield('content')
</div>
<!--JavaScript at end of body for optimized loading-->

<script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/funcoes.js')}}"></script>
</body>
</html>