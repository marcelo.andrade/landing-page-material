@extends('layouts.comum')

@section('content')
    <section >
        <div class="container">
            <div class="row">
                <div class="col s8 desc">
                    <h1>Landing Page Material</h1>
                    <h2>This is my Landing Page whith Material CSS course</h2>
                    <a href="#" class="btn-large waves-effect waves-light">Mensagem</a>
                </div>
            </div>
        </div>

        <div class="parallax-container">
            <div class="parallax"><img src="img/fundo-03.jpg" class="responsive-img"></div>
        </div>
        <div class="section white">
            <div class="row container">
                <h2 class="header">Parallax</h2>
                <p class="grey-text text-darken-3 lighten-3">Parallax is an effect where the background content or image in this case, is moved at a different speed than the foreground content while scrolling.</p>
            </div>
        </div>
        <div class="parallax-container">
            <div class="parallax"><img src="img/fundo-02.jpg" class="responsive-img"></div>
        </div>
    </section>

    <div class="fixed-action-btn">

        <a href="#" class="waves-effect purple accent-3 btn btn-floating right"><i class="material-icons">arrow_drop_up</i></a>
    </div>
    <section class="feature cyan darken-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col s12 desc">
                    <h2>Features</h2>
                </div>
            </div>
        </div>
    </section>
@stop